<?php 
	require_once("functions.php"); 
	getHeader();
	// Transformation des query string en variables
	parse_str($_SERVER['QUERY_STRING']);
	// Ex : si l'URL est de la forme update.php?id=10 alors $id vaut désormais 10.
?>
<h1>Mise à jour d'un prospect</h1>
<?php if(isset($id)) : ?>
	<a href="update.php">Editer un autre prospect</a>
	
	<?php if(isset($_POST["submit"])) : ?>
		<?php 
			// On vérifit que l'utilisateur a bien entré tout les champs
			$warnings = array(); 
			if(isset($_POST["firstname"]) && empty($_POST["firstname"]))
			{
				$warnings[] = "Veuillez indiquer le <strong>prénom</strong> du contact.";
			}	
			if(isset($_POST["lastname"])  && empty($_POST["lastname"]))
			{
				$warnings[] = "Veuillez indiquer le <strong>nom</strong> du contact.";
			}
			if(isset($_POST["mail"])  && empty($_POST["mail"]))
			{
				$warnings[] = "Veuillez indiquer l'<strong>adresse mail</strong> du contact.";
			}
		?>
		<?php if(count($warnings) == 0) : updateContact($_POST["id"], $_POST["firstname"], $_POST["lastname"], $_POST["mail"]);?>
			<div class="alert alert-success">
				Le prospect a bien été mis à jour !
			</div>
		<?php else : ?>
			<div class="alert alert-danger">
				<ul>
				<?php foreach ($warnings as $warning) : ?>
					<li><?php echo $warning; ?></li>
				<?php endforeach; ?>
				</ul>
			</div>
		<?php endif; ?>
	<?php endif; ?>
	<?php $prospect = readOneContact($id); ?>
	<form method="post" role="form">
		<div class="input-group">
		  <span class="input-group-addon">Prénom</span>
		  <input type="text" class="form-control" name="firstname"  value="<?php echo $prospect["firstname"] ?>" >
		</div>
		<div class="input-group">
		  <span class="input-group-addon">Nom</span>
		  <input type="text" class="form-control" name="lastname" value="<?php echo $prospect["lastname"] ?>">
		</div>
		<div class="input-group">
		  <span class="input-group-addon">Mail</span>
		  <input type="text" class="form-control" name="mail" value="<?php echo $prospect["mail"] ?>">
		</div>
		<input type="hidden" name="id" value="<?php echo $prospect["id"] ?>">
		<div class="input-group  pull-right">
			<input type="submit" name="submit" class="btn btn-primary" value="Sauvegarder">
		</div>
		<style>
			form, .alert {
				width:500px;
			}

			span {
				display: block;
				width:80px;
			}

			.input-group {
				margin-bottom:21px;
			}
		</style>
	</form>
<?php else : ?>
	<form method="get">
		<select name="id">
			<?php 
				$prospects = readAllContact();
				foreach ($prospects as $prospect) : ?>
					<option value="<?php echo $prospect["id"] ?>"><?php echo $prospect["firstname"]." ".$prospect["lastname"]; ?></option>	
				<?php endforeach;?>
		</select>
		<input type="submit" class="btn btn-primary" value="Editer">
	</form>
<?php endif; ?>

<?php getFooter(); ?>