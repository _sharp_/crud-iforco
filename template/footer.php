	</div> <!-- .container -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="http://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script>
    	// Petit script jquery pour ajouter la classe active au menu pour la page courante
    	// On pourrait le faire en PHP mais ça alourdirait le code inutilement 
    	$(document).ready(function() {
    		var pathname = window.location.pathname;
    		if(pathname.indexOf('index.php') != -1 )
    		{
    			$("#menu li:eq(0)").addClass('active');
    		}
    		if(pathname.indexOf('create.php') != -1)
    		{
    			$("#menu li:eq(1)").addClass('active');
    		}
    		if(pathname.indexOf('read.php') != -1)
    		{
    			$("#menu li:eq(2)").addClass('active');
    		}
    		if(pathname.indexOf('update.php') != -1)
    		{
    			$("#menu li:eq(3)").addClass('active');
    		}
    		if(pathname.indexOf('delete.php') != -1)
    		{
    			$("#menu li:eq(4)").addClass('active');
    		}
    	});
    </script>
  </body>
</html>