<!DOCTYPE html>
<html>
  <head>
    <title>Create, Read, Update, Delete</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- On utilise le framework CSS Bootstrap (http://getbootstrap.com/) -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="css/style.css" rel="stylesheet" media="screen">
  </head>
  <body>
  	<?php include('template/menu.php'); ?>
  	<div class="container">