<?php
	// -------------------
	// Fonctions de thème
	// -------------------
	function getHeader() {
		include('template/header.php');
	}

	function getFooter() {
		include('template/footer.php');
	}

	// --------------------------
	// Accès  la base de données
	// --------------------------
	require_once('config.php');
	function readAllContact() {
		$sql = "SELECT * FROM prospects";
		$res = ExecuteQuery($sql);
		return $res;
	}

	function readOneContact($id) {
		$sql = "SELECT * FROM prospects WHERE id=".$id;
		$res = ExecuteQuery($sql);
		return $res[0];
	}

	function updateContact($id, $firstname, $lastname, $mail) {
		$sql = "UPDATE prospects SET firstname='".$firstname."', lastname='".$lastname."', mail='".$mail."' WHERE id=".$id;
		return ExecuteNoQuery($sql);
	}

	function deleteContact($id) {
		$sql = "DELETE FROM prospects WHERE id=".$id;
		return ExecuteNoQuery($sql);
	}

	function insertContact($firstname, $lastname, $mail) {
		$sql = "INSERT INTO prospects(`firstname`,`lastname`,`mail`) VALUES('".$firstname."', '".$lastname."', '".$mail."')";
		return ExecuteNoQuery($sql);
	}

	// Fonction utilitaire pour afficher les messages d'erreurs
	function displayError($msg) {
		die('<div class="alert alert-danger">'.$msg.'</div>');
	}

	// Fonction utilitaire pour executer des requêtes Mysql qui retourne un résultat (SELECT)
	function ExecuteQuery($sql){
		$liaison = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);
		mysql_select_db (DB_DATABASE, $liaison) or displayError('Erreur MYSQL ' . DB_DATABASE . ' : ' . mysql_error());
		$query = mysql_query($sql);
		if(!$query)
		{
			displayError('Erreur MYSQL : ' . mysql_error());
		}
		else {
			$result = Array();
			while($item = mysql_fetch_assoc($query))
			{
				$result[] = $item;
			}
			mysql_close($liaison);
			return $result;
		}
	}

	// Fonction utilitaire pour executer des requêtes Myqsl qui ne retourne pas de résultat (INSERT, UPDATE, DELETE) 
	function ExecuteNoQuery($sql)
	{
		$liaison = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);
		mysql_select_db (DB_DATABASE, $liaison) or displayError('Erreur MYSQL ' . DB_DATABASE . ' : ' . mysql_error());
		$query = mysql_query($sql);
		if(!$query)
		{
			displayError('Erreur MYSQL : ' . mysql_error());
		}
		else {
			mysql_close($liaison);
		}
	}

?>