<?php 
	require_once("functions.php"); 
	getHeader();
?>

<h1>CRUD (Create, Read, Update, Delete)</h1>
<p>Création d'un outil de gestion des prospects en PHP. </p>
<h2>Création de la base de données</h2>
<p>Pour faire fonctionner cet exemple : </p>
<ol>
	<li>Créer la base de données dans phpMyAdmin grâce a <a href="prospectsdb.sql">ce script sql</a>
	<li>Mettre à jour le fichier <i>configuration.php</i> situé à la racine du site avec les informations de connexion à la base de données</li>
</ol> 
<h2>Formulaires PHP</h2>
<p>Une fois que la base de données et la table prospect ont été crées vous pouvez utiliser les formulaires php suivants pour mettre à jour la liste des prospects :</p>
<ul>
	<li>Create : /create.php</li>
	<li>Read : /read.php</li>
	<li>Update : /update.php</li>
	<li>Delete : /delete.php</li>
</ul>
Ceci est un test !
<?php getFooter(); ?>