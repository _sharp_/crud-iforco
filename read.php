<?php 
	require_once("functions.php"); 
	getHeader();
	// Transformation des query string en variables
	parse_str($_SERVER['QUERY_STRING']);
	// Ex : si l'URL est de la forme read.php?id=10 alors $id vaut désormais 10.
?>
<h1>Lecture de la base de données</h1>

<?php if(isset($id)) : // Lecture des données d'un prospect, url de la forme read.php?id=XX ?>
	<?php $prospect = readOneContact($id); ?>
	<a href="read.php">Retour à la liste des prospects</a>
	<dl>
	  <dt>Id</dt>
	    <dd><?php echo $prospect["id"]; ?></dd>		
	  <dt>Prénom</dt>
	    <dd><?php echo $prospect["firstname"]; ?></dd>
	  <dt>Nom</dt>
	    <dd><?php echo $prospect["lastname"]; ?></dd>
	  <dt>Mail</dt>
	    <dd><?php echo $prospect["mail"]; ?></dd>
	</dl>
<?php else : // Lecture de tout les prospects de la bases, url de la forme read.php ?>
	<?php $prospects = readAllContact();?>
	<table class="table">
		<tr>
			<th>#</th>
			<th>Prénom</th>
			<th>Nom</th>
			<th></th>
		</tr>
		<?php foreach($prospects as $prospect) : ?>
		<tr>
			<td><?php echo $prospect["id"]?></td>
			<td><?php echo $prospect["firstname"]?></td>
			<td><?php echo $prospect["lastname"]?></td>
			<td>
				<a href="read.php?id=<?php echo $prospect["id"] ?>">Détails</a> | 
				<a href="update.php?id=<?php echo $prospect["id"] ?>">Editer</a> | 
				<a href="delete.php?id=<?php echo $prospect["id"] ?>">Supprimer</a>
			</td>
			
		</tr>
		<?php endforeach; ?>
	</table>
<?php endif; ?>

<?php getFooter(); ?>