<?php 
	require_once("functions.php"); 
	getHeader();
	// Transformation des query string en variables
	parse_str($_SERVER['QUERY_STRING']);
	// Ex : si l'URL est de la forme delete.php?id=10 alors $id vaut désormais 10.
?>
<h1>Suppression d'un prospect</h1>

<?php if(isset($id) && !isset($_POST["submit"])) : ?>
	<p>Êtes-vous sûr de vouloir supprimer ce contact ?</p>
	<form method="POST">
		<input type="submit" name="submit" class="btn btn-primary" value="Supprimer">
	</form> 
<?php else : ?>
	<?php if(isset($_POST["submit"])) : ?>
		<?php deleteContact($id); ?>
		<div class="alert alert-success">
			Le prospect a bien été supprimé !
		</div>
	<?php endif; ?>
	<form method="get">
		<select name="id">
			<?php 
				$prospects = readAllContact();
				foreach ($prospects as $prospect) : ?>
					<option value="<?php echo $prospect["id"] ?>"><?php echo $prospect["firstname"]." ".$prospect["lastname"]; ?></option>	
				<?php endforeach;?>
		</select>
		<input type="submit" class="btn btn-primary" value="Supprimer">
	</form> 
<?php endif; ?> 

<?php getFooter(); ?>