<?php 
	require_once("functions.php"); 
	getHeader();
?>
<h1>Création d'un nouveau prospect</h1>
<?php if(isset($_POST["submit"])) : ?>
	<?php 
		// On vérifit que l'utilisateur a bien entré tout les champs
		$warnings = array(); 
		if(isset($_POST["firstname"]) && empty($_POST["firstname"]))
		{
			$warnings[] = "Veuillez indiquer le <strong>prénom</strong> du contact.";
		}
		if(isset($_POST["lastname"])  && empty($_POST["lastname"]))
		{
			$warnings[] = "Veuillez indiquer le <strong>nom</strong> du contact.";
		}
		if(isset($_POST["mail"])  && empty($_POST["mail"]))
		{
			$warnings[] = "Veuillez indiquer l'<strong>adresse mail</strong> du contact.";
		}
	?>
	<?php if(count($warnings) == 0) : ?>
		<?php if(insertContact($_POST["firstname"], $_POST["lastname"], $_POST["mail"])) : ?>
			<div class="alert alert-success">
				Le nouveau prospect a bien été crée !
			</div>
		<?php endif; ?>
	<?php else : ?>
		<div class="alert alert-danger">
			<ul>
			<?php foreach ($warnings as $warning) : ?>
				<li><?php echo $warning; ?></li>
			<?php endforeach; ?>
			</ul>
		</div>
	<?php endif; ?>
<?php endif; ?>
<form method="post" role="form">
	<div class="input-group">
	  <span class="input-group-addon">Prénom</span>
	  <input type="text" class="form-control" name="firstname" value="<?php if(isset($_POST["firstname"])) echo $_POST["firstname"]?>" >
	</div>
	<div class="input-group">
	  <span class="input-group-addon">Nom</span>
	  <input type="text" class="form-control" name="lastname" value="<?php if(isset($_POST["lastname"])) echo $_POST["lastname"]?>">
	</div>
	<div class="input-group">
	  <span class="input-group-addon">Mail</span>
	  <input type="text" class="form-control" name="mail"  value="<?php if(isset($_POST["mail"])) echo $_POST["mail"]?>">
	</div>
	<div class="input-group  pull-right">
		<input type="submit" name="submit" class="btn btn-primary" value="Créer">
	</div>
	<style>
		form, .alert {
			width:500px;
		}

		span {
			display: block;
			width:80px;
		}

		.input-group {
			margin-bottom:21px;
		}
	</style>
</form>
<?php getFooter(); ?>